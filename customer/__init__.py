from flask import Flask
import configparser
import os
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# Read config file
config = configparser.ConfigParser()
basedir = os.path.abspath(os.path.dirname(__file__))
config.read(os.path.join(basedir, 'customer_db.conf'))

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + config.get('DB', 'user') + \
                                        ':' + config.get('DB', 'password') + '@' + \
                                        config.get('DB', 'host') + '/' + config.get('DB', 'db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


from . import rest
