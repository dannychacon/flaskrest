from flask import jsonify
from customer import app
from customer.models import Customer


def convert(data):
    data_all = []
    for e in data:
        data_all.append({'id': e.id,
                         'name': e.name,
                         'address': e.address
                         })
    return data_all


@app.route('/customers')
def test2():
    return jsonify(convert(Customer.query.all()))

@app.route('/')
def hello_world():
    return 'Customer app!'