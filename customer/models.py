from customer import db


# map models
class Customer(db.Model):
    __tablename__ = 'customer'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    address = db.Column(db.String(128), nullable=False)

    def __repr__(self):
        return '<Customer (%s, %s) >' % (self.id, self.name)
