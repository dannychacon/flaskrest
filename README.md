
### What is this repository for? ###

* Demo REST API with flask
* 0.1

### How do I get set up? ###

* Install dependencies:
	pip install -r requirements.txt

### Contribution guidelines ###

* After added new dependencies
	pip freeze > requirements.txt